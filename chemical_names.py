import re
from typing import Union, Any


def from_string(name: str, ice: bool = True, ortho: bool = True, para: bool = True):
    """Convert chemical or in-ANDES notation to LaTeX"""

    two_letter_atoms = ["Fe", "Mg", "Cl", "Si", "Na", "He"]
    for two_letter_atom in two_letter_atoms:
        name = name.replace(two_letter_atom.upper(), two_letter_atom)

    atoms = rf"(?:{'|'.join(two_letter_atoms)}|H|D|C|O|N|P|S)"
    name = name.replace("Mg+ + Na+ + Fe+", "M+")
    name = name.replace("oH2D+ + pH2D+ + 2 * oHD2+ + 2 * pHD2+ + 3 * oD3+ + 3 * pD3+ + 3 * mD3+", "D in H3+")
    name = name.replace("n(H+2H2)", "<H>")
    name = name.replace('ELECTR', 'e$^-$')
    name = name.replace('Tg', 'Gas temperature')
    name = name.replace('Td', 'Dust temperature')
    name = name.replace("<", r"$\langle{}$")
    name = name.replace(">", r"$\rangle{}$")
    name = re.sub(r'm(\S+)\s+\+\s+o\1\s+\+\s+p\1', r'\1', name)
    name = re.sub(r'o(\S+)\s+\+\s+p\1\s+\+\s+m\1', r'\1', name)
    name = re.sub(r'o(\S+)\s+\+\s+p\1', r'\1', name)
    name = re.sub(r'p(\S+)\s+\+\s+o\1', r'\1', name)
    name = re.sub(r'(\S)\+', r'\1$^+$', name)
    name = name.replace('13C', '$^{13}$C')
    name = name.replace('18O', '$^{18}$O')
    name = re.sub(rf"({atoms})(\d)", r"\1$_\2$", name)
    if ice: name = re.sub(rf"\bg([\S]*{atoms}+[\S]*)", r"\1 ice", name)
    if ortho: name = re.sub(rf"\bo(.*{atoms}+.*)", r"ortho-\1", name)
    if para: name = re.sub(rf"\bp(.*{atoms}+.*)", r"para-\1", name)
    name = name.replace("$$", "")
    return name


def from_data_or_string(data: Union[str, Any], ignore_if='{$}'):
    """
    Function to get printable LaTeX name from column or string

    :param data: entry key or astropy.table.Column
    :param ignore_if: return string without changes if one of symbols is present
    :return: string with printable name
    """
    pre_converted = "None"
    if data is None:
        return "None"
    try:
        if isinstance(data, str):
            pre_converted = data
        else:
            pre_converted = data.name
    except AttributeError:
        return "Undefined"
    if set(ignore_if).intersection(data):
        return pre_converted
    else:
        return from_string(pre_converted)
