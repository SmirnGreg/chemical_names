from setuptools import setup, find_packages

setup(
    name='chemical_names',
    py_modules=['chemical_names'],
    url='https://gitlab.com/smirngreg/chemical_names',
    license='MIT',
    author='Grigorii V. Smirnov-Pinchukov',
    author_email='smirnov@mpia.de',
    description='Tool to convert chemical name into LaTeX',
    version_config=True,
    setup_requires=['setuptools-git-versioning'],
    python_requires=">=3.4",
)
